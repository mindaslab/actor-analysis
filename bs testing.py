from urllib.request import urlopen
from contextlib import closing
from bs4 import BeautifulSoup
import pandas as pd

Actor = pd.DataFrame({"Title": [], "Rating": [], "Director": [], "Genre": [], "Year": [], "Duration": []})
TitleList, RatingList, DirectorList, GenreList, YearList, DurationList = [], [], [], [], [], []

movieDict = dict()
# url = "https://www.imdb.com/name/nm4043111/" #Vijay Sethupathi
# url = "https://www.imdb.com/name/nm0897201/?ref_=nv_sr_1" #Vijay
# url = "https://www.imdb.com/name/nm0707425/?ref_=nv_sr_1" #Rajini
# url = "https://www.imdb.com/name/nm0352032/?ref_=nv_sr_5" #kamal
# url="https://www.imdb.com/name/nm0015001/" #ajith
url = "https://www.imdb.com/name/nm0304262/" #Shivaji


html = urlopen(url)
soup = BeautifulSoup(html, 'html.parser')
movieLinks = []
for i in soup.findAll("div", {"class": "filmo-category-section"}):
    for a in i.find_all('a', href=True):
        movieDict[a.string] = a['href']
print(len(movieDict))


for index,eachMovie in enumerate(movieDict.values()):
    # if index >12:
        # if movieDict['pre-production'] != eachMovie:                         # FOR Vijay,Kamal,Ajith
            #if movieDict['filming'] != eachMovie:
            # if index < 215:                                                    #FOR RAJINI,VijaySethupathi
                # if movieDict['post-production'] != eachMovie:             # FOR VS
                # if movieDict["Rajyaveeran"] != eachMovie:                   #FOR RAJINI
                                                                            #FOR RAJINI
                    # if movieDict['completed'] != eachMovie:               # FOR VS
                    # if movieDict['announced'] != eachMovie:               # FOR RAJINI

                        url = "https://www.imdb.com" + eachMovie
                        html = urlopen(url)
                        soup = BeautifulSoup(html, 'html.parser')
                        # for credit_summary in soup.findAll("div",{"class":"credit_summary_item"}):
                        director = soup.findAll("div", {"class": "credit_summary_item"})[0].findAll("a", href = True)[0].string
                        DirectorList.append(director)
                        for title_block in soup.findAll("div", {"class":"title_block"}):
                            rating =  title_block.findAll("span", {"itemprop": "ratingValue"})
                            year = title_block.findAll("span", {"id": "titleYear"})
                            duration = title_block.findAll("time", datetime = True)
                            title = title_block.findAll("h1", {"class": ""})
                            genre = title_block.findAll("div", {"class": "subtext"})
                            if rating:
                                rating = rating[0].string
                            else:
                                rating = "NAN"
                            if year:
                                for year in year:
                                    year = year.findAll("a",href=True)[0].string
                            else:
                                year = "NAN"
                            if duration:
                                duration = duration[0].string.replace("\n", "").replace(" ", "")
                            else:
                                duration = "NAN"
                            if title:
                                for title in title:
                                    spans = title.findAll("span", {"id": "titleYear"})
                                for span in spans:
                                    span.decompose()
                                title = title.get_text()
                            else:
                                title = "NAN"
                            if genre:
                                for genre in genre:
                                    spans = genre.findAll("span", {"class": "ghost"})
                                    aa = genre.findAll("a", href=True)
                                    times = genre.findAll("time", datetime=True)
                                for span in spans:
                                     span.decompose()
                                for a in aa:
                                     a.decompose()
                                for time in times:
                                     time.decompose()
                                genre = genre.get_text().replace("\n", "").replace(" ", "").replace(",", "")
                                if not genre:
                                    genre = "NAN"
                            else:
                                genre = "NAN"
                            RatingList.append(rating)
                            YearList.append(year)
                            GenreList.append(genre)
                            DurationList.append(duration)
                            TitleList.append(title)
                            print('*****************************************{}--{}--{}--{}'.format(director, genre, title, index))

Actor["Title"] = TitleList
Actor["Rating"] = RatingList
Actor["Director"] = DirectorList
Actor["Genre"] = GenreList
Actor["Year"] = YearList
Actor["Duration"] = DurationList
Actor.to_csv("~/Shivaji", index=False)
print(Actor.head(5))
